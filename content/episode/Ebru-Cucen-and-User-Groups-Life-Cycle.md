+++
Description = "This episode we interview Ebru Cucen to talk about meetups, user groups, and getting involved with the community - Should I go and listen? Should I submit a talk? Should I try coordinating a group? We'll answer these questions and chat with the community at large."
PublishDate = "2017-08-01"
aliases = ["/usergroups"]
author = "Mike"
date = "2017-08-01"
episode = "5"
episode_image = "img/logo.jpg"
explicit = "yes"
guests = ["ecucen"]
images = ["img/episode/default-social.jpg"]
podcast_bytes = ""
podcast_duration = "53:45"
podcast_file = ""
sponsors = []
title = "Ebru Cucen + User Groups Life Cycle"
youtube = "uJUUP9OAvdM"

+++

+ [Ebru's Lifecycle Document](/img/Lifecycle.pdf)
+ [PowerShell Day](https://psday.uk)
+ [London PowerShell User Group](https://www.meetup.com/PowerShell-London-UK/)
