+++
Description = "This episode we interview Emily Gladstone Cole to learn about the basics of securing corporate systems, hear her plea to patch all the things, and then chat with the community at large."
PublishDate = "2017-04-05"
aliases = ["/security"]
author = "Mike"
date = "2017-04-05"
episode = "2"
episode_image = "img/logo.jpg"
explicit = "yes"
guests = ["egladstonecole"]
images = ["img/episode/default-social.jpg"]
podcast_bytes = ""
podcast_duration = "1:04:08"
podcast_file = ""
sponsors = []
title = "Emily Gladstone Cole + Security"
youtube = "12jUXfbva-s"

+++

[**Emily's Summary on Twitter**](https://twitter.com/unixgeekem/status/850017832150618112)

#### Referenced Links:

+ https://www.wired.com/2016/01/nsa-hacker-chief-explains-how-to-keep-him-out-of-your-system/
+ [Center for Internet Security](https://www.cisecurity.org/)
+ [DISA STIGs](http://iase.disa.mil/stigs/Pages/index.aspx)

#### Links from Emily:

+ [USENIX Enigma 2016 - NSA TAO Chief on Disrupting Nation State Hackers](https://www.youtube.com/watch?v=bDJb8WOJYdA)
+ [Open Web Application Security Project (OWASP)](https://www.owasp.org/index.php/Main_Page)
+ [The Six Stages of Incident Response](https://www.cso.com.au/article/600455/six-stages-incident-response/)
+ [Ten Secure Coding Practices](https://www.securecoding.cert.org/confluence/display/seccode/Top+10+Secure+Coding+Practices)
+ [IT and Information Security Cheat Sheets](https://zeltser.com/cheat-sheets/)