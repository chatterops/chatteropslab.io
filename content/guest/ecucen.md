+++
Instagram = ""
GitHub = "ebrucucen"
Linkedin = "ebrucucen"
Facebook = ""
Website = "https://continuousdevelopment.wordpress.com/"
title = "Ebru Cucen"
YouTube = ""
Type = "guest"
Twitter = "ebrucucen"
Pinterest = ""
Thumbnail = "https://pbs.twimg.com/profile_images/770542843274354688/3t2VTngI_400x400.jpg"
date = "2017-07-12T14:44:30-05:00"

+++

Ebru is passionate about automation and enjoys the luxury being part of a great community that shares their knowledge and experience.
She is co-organiser of the [London PowerShell User Group][lonpsug].
Since the success of the [Halfday PowerShell][halfday] event in June she has been busy with coordinating a one-day conference event, _PSDay London_, if not planning a visit to Jupiter (or anywhere interesting) with her almost 5 years old son and husband.

[lonpsug]: https://www.meetup.com/PowerShell-London-UK/
[halfday]: https://powershell.org.uk/2017/05/13/half-day-powershell/