# Transcripts

We are working to get transcripts for each episode and will include them either embedded into the episode page or in an obvious link from that page and on this one.

We're looking forward to having a better process for this, thank you for bearing with us!